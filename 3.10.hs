esSegmento xs ys 
           | length xs > length ys = False
           | h1==h2 && xs == take (length xs) ys = True 
           | otherwise = esSegmento xs t2
           where (h1:_,h2:t2)=(xs,ys)

esSegmento2 xs ys | length xs > (length ys)   = False
                  | xs == take (length xs) ys = True
                  | otherwise                 = esSegmento2 xs (tail ys)