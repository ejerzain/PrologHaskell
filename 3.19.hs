? segcrec [1,2,3,4,2,3,5,6,4,8,3,2]
[[1,2,3,4],[2,3,5,6],[4,8],[3],[2]]-}
lst 3.19=[1,2,3,4,2,3,5,6,4,8,3,2]
segrec []=[]
segrec (h:t)=let f lst x | x>=(last.last$lst)=(init lst)++[last lst++[x]]
                         | otherwise=lst++[[x]] 
             in  foldl f [[h]] t

segrec1 lst=let f x []=[[x]]
                f x (h':t') | x<(head h') = (x:h'):t'
                            | otherwise=[x]:(h':t')
            in foldr f [] $ lst

segrec2 []=[]
segrec2 [h]=[[h]]
segrec2 (h:h':t)|h>h'=[h]:next
                |otherwise=(h:(head next)):(tail next)
                where next=segrec2 $ h':t
