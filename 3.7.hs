ndedc:: (Eq a) => [a] -> Int
ndedc lista = let  norep [] n=[n]
                   norep (x:xs) n | x == n    = x:xs
                                  | otherwise = n:x:xs
              in length $ foldl norep [] lista 