-- Esta funcion es una generalizacion del ejercicio 3.17 y se pueden trasladar las soluciones
-- directamente poniendo el predicado como parametro
partir=span
partir1 pred xs= (takeWhile pred xs,dropWhile pred xs)  
partir2 pred xs=until (pred.head.snd) (\(ys,(h:t))->(ys++[h],t)) ([],xs)
--Pero nos la piden con foldl y/o foldr
partirL pred xs=let nxt (xs,ys) x
			| pred x=(xs,ys++[x])
			| otherwise =(xs++[x],ys)
		in foldl nxt ([],[]) xs

partirR pred xs=let nxt x (xs,ys) |pred x=(xs,x:ys)
				  |otherwise=(x:xs,ys)
		in foldr nxt ([],[]) xs