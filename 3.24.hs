listaNumR k xs=let f x (acc,0)=(acc+x,0)
		   f x (acc,i)=(acc+(x*k^i),i-1)
	       in  fst $ foldr f (0,(length xs)-1) xs	   

-- Solucion mas elegante de Xavier Garcia Buils
porKmas k m n = m*k+n
listaAnumero k = foldl (porKmas k) 0
listaAnumeroR k = foldr (flip $ porKmas k) 0
