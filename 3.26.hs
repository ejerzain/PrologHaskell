n % m = (div n m, mod n m)
multip nm mm = reverse(let m (acc,r) y =
                               	 let (d,e) = (y * (sum nm) + r) % 10 
                               	 in (e:acc,d) 
                         in uncurry (flip (:)) (foldl m ([],0) mm))
