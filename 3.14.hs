-- La mia

extremos [h]= []
extremos [h,t]=[]
extremos (h:t)= 
         let f (acc,p) x 
               | x>p && (head acc)>p ||  
                 x<p && (head acc)<p=(p:acc,x)
               | otherwise= (acc,x)
         in init.fst $ foldl f ([h],h) t

extremos1 [h]= []
extremos1 [h,t]=[]
extremos1 (h:t)=let f (acc,p) x = (acc++e,x) 
                     where e | x>p && (last acc)>p ||  
                               x<p && (last acc)<p=[p]
                             | otherwise=[]
               in tail.fst $ foldl f ([h],h) t
--Otra
extremos2:: [Int] -> [Int]
extremos2 lista = fst (foldl f ([], []) lista)
                 where f ([], []) n     = ([], [n])
                       f (extremos, (y:ys)) n | ys == [] && y == n =(extremos, (y:ys ))
                                              | ys == [] && y /= n =(extremos, (y:[n]))
                                              | head ys == n =(extremos, (y:ys))
                                              | (y < head ys)==((head ys) < n)=(extremos, y:[n])
                                              | otherwise =(extremos ++ [head ys], (head ys):[n])

