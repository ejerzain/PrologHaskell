infix  6  :+
data  (RealFloat a) => Complex a = !a :+ !a  deriving (Eq,Read,Show)
instance  (RealFloat  a) => Num (Complex a)  where
    (x:+y) + (x':+y') =  (x+x') :+ (y+y')
    (x:+y) - (x':+y') =  (x-x') :+ (y-y')
    (x:+y) * (x':+y') =  (x*x'-y*y') :+ (x*y'+y*x')
    abs z =  undefined
    signum z = undefined  
    fromInteger n = fromInteger n :+ 0

dividirCompl:: Complex Float-> Complex Float-> Complex Float
dividirCompl (x:+y) (x':+y') = (x:+y) * ((x'/div):+(-y'/div))
                            	 where div = x'*x' + y'*y'