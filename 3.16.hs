sc lista = combinacion lista
           where combinacion     [] = []
                 combinacion (x:xs) = combinacion xs 
                                      ++ (combina x $ combinacion xs) 

combina elemento []     = [[elemento]]
combina elemento (x:xs) | x == [] = combina elemento xs
                        | elemento <=head x=(elemento:x):(combina elemento xs )
                        | otherwise=combina elemento xs  

