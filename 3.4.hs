assert 3.4 f lst=(map (+ 1).reverse $ lst)
                  == (f lst)
test 3.41 = assert 3.4 (foldl (\a b->b+1:a) []) [0,1,2,3]