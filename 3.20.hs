esSubLista xs []=True
esSubLista [] ys=False
esSubLista (h:t) (h':t')
		 | (h:t)==(h':t')=True
		 | h==h'=esSubLista (h:t) t'
		 | otherwise = esSubLista t (h':t')
