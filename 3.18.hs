strt="Cuales son las letras unicas en esta frase?"
unico str=let low=map toLower str;nuby=nub low
	   in nuby\\(low\\nuby)
unico1 []=[]
unico1 ls=let low=map toLower ls
	      f acc []=acc
	      f acc (h:t)
		|elem h t =f acc flt
		|otherwise=f (acc++[h]) flt
		where flt=filter (/= h) t     
	  in f [] low
unico2=concat.(filter ((== 1).length)).group.sort.(map toLower)

