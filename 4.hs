dividir lst=let line x (h:t) |x==h=x:'\n':h:t
                             |otherwise=x:h:t
                f (h:t)=foldr line [last (h:t)] $ init (h:t)
                f []=[]    
            in putStrLn $ f lst  