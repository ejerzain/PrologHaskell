distanciaExtr (h:t)= 
         let f ((i,max),(acc,p)) x 
                | x>p && (head acc)>p ||  
                  x<p && (head acc)<p=((0,max'),(p:acc,x))
                | otherwise= ((i',max),(acc,x))
               where i'=i+1
                     max' | max<0=0 | i'>max=i'  
                          |otherwise=max
         in  snd.fst $ foldl f ((0,-1),([h],h)) t

-- Entendi mal no es la distancia segun el indice del extremo sino la diferencia entre valores:
distanciaExtr2:: [Int] -> Int
distanciaExtr2 lista = maxDif (extremos lista)

-- Maxima diferencia absoluta entre dos elementos consecutivos
maxDif:: [Int] -> Int
maxDif (x:xs) | length (x:xs) < 2=0
              | otherwise=fst ( foldl f (0, x) xs )
                 where f (a, b) n=( max (abs(b-n))a, n )
