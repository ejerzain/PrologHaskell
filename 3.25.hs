x % y = (div x y,mod x y)
mult x xs=let m (acc,r) y=
                let (d,r')=(y*x+r)%10 
                in (r':acc,d) 
          in uncurry (flip (:)) $ foldl m ([],0) xs 
           
multR2 x xs=let m y (acc,r)=
                  let (d,r')=(y*x+r)%10 
                  in (r':acc,d) 
            in uncurry (flip(:)) $ foldr m ([],0) xs