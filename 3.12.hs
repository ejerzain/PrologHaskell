sssp lst = let k=length lst
               f xs=let n=length xs
                    in xs++[(sum.drop(n-k).take n) xs] 
           in init lst ++ (map last $ iterate f lst)

sssp2:: [Int] -> [Int]
sssp2 xs = xs ++ map fst ( iterate f (sum xs, xs) )
          where f (suma, y:ys) = (sum zs, zs) where zs = ys ++ [suma]

-- La buena 
sssp3 ns = map head (iterate f ns)
  where f ns = (tail ns) ++ [sum ns]
  