segmento2 i j lst 
         | j > (length lst)=segmento i (length lst) lst
         | i>=j = segmento j i lst
         | otherwise= let acc (c,xs) x | c>=i && c<=j = (c+1,xs++[x])
                                       | otherwise = (c+1,xs)
                      in snd $ foldl acc (0,[]) lst    

segmento:: Int -> Int ->  [a] -> [a]
segmento i j lista | i >= j = segmento2 j i lista
                    | i < 0=segmento2  0 j lista
                    | j > l=segmento2  i l lista
                    | otherwise=take (j-i) (drop i lista)
                    where l=length lista